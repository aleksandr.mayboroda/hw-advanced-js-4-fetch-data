
//классом не вышло: вызов до инициализации класса невозможен
function Preloader(parent)
{   
    this.elements = {
        parent: parent,
        block: document.createElement('div'),
        wrapper: document.createElement('div'),
        top_text: document.createElement('p'),
        bottom_text: document.createElement('p')
    }
    this.show = function()
    {
        let {parent,block,wrapper,top_text,bottom_text} = this.elements

        block.classList = 'preloader'
        wrapper.classList = 'preloader__inner'
        top_text.classList = 'preloader__top'
        bottom_text.classList = 'preloader__bottom'

        top_text.innerText = 'star'
        bottom_text.innerText = 'wars'
        
        wrapper.append(top_text,bottom_text)
        block.append(wrapper)
        parent.insertAdjacentElement('beforeend',block)

    }
    this.hide = function()
    {
        let {block} = this.elements
        block.remove()
    }
}