class Film
{
    constructor({id,name,episodeId,openingCrawl,characters})
    {
        this.id = id
        this.title = name
        this.episodeId = episodeId
        this.openingСrawl = openingCrawl
        this.characters = characters

       if(characters)
       {
           this.getCharacters(characters)
       }

        // console.log(this)
    }
    elements = {
        // filmList: document.createElement('div'),
        filmItem: document.createElement('div'),
        title: document.createElement('h2'),
        episodeId: document.createElement('h4'),
        openingСrawl: document.createElement('p'),
        charactersBlock: document.createElement('div'),
        characters: document.createElement('ul'),
    }

    render(parentElem)
    {
        const {filmItem,title,episodeId,openingСrawl,characters,charactersBlock} = this.elements

        // filmList.className = 'film'
        filmItem.className = 'film__item'

        title.innerText = this.title
        title.className = 'film__title'

        episodeId.innerText = `Episode# ${this.episodeId}`
        episodeId.className = 'film__episode'

        openingСrawl.innerHTML = this.openingСrawl
        openingСrawl.className = 'film__description-short'

        //title
        // characters.innerText = this.characters
        charactersBlock.className = 'film__characters-block'
        charactersBlock.insertAdjacentHTML('afterbegin',`<h5>Characters:</h5>`)
        
        characters.className = 'film__characters-list'

        charactersBlock.append(characters)

        filmItem.append(title,episodeId,openingСrawl)
        // filmList.append(filmItem)
        parentElem.insertAdjacentElement('beforeend',filmItem)
    }

    getCharacters(charactersUri)
    {
        // console.log(charactersUri)
        if(charactersUri.length > 0)
        {
            let {title,characters,charactersBlock} = this.elements
            let promises = charactersUri.map(charUrl => {
                return fetch(`${charUrl}`,{
                    method: 'get',
                    headers: heads
                })
                .then(resp => resp.json())
            })

            Promise.all(promises)
            // .then(resp => console.log(resp))
            .then(resp => {
                if(resp.length > 0)
                {
                    resp.forEach(ch => {
                        const char = new Character(ch)
                        char.render(characters)
                        charactersBlock.append(characters)
                        title.after(charactersBlock)
                    });
                }
            })
        }
    }


}