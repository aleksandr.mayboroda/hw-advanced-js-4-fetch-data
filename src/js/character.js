class Character
{
    constructor({id,name,gender,birthYear})
    {
        this.id = id
        this.name = name
        this.gender = gender || ' - '
        this.birthYear = birthYear || ' - '
    }

    elements = {
        character: document.createElement('li'),
        name: document.createElement('h4'),
        gender: document.createElement('p'),
        birthYear: document.createElement('p'),
    }

    render(parentElem)
    {
        let {character,name,gender,birthYear} = this.elements
            
        character.className = 'character'
        name.className = 'character__name'
        gender.className = 'character__gender'
        birthYear.className = 'character__birth'

        name.innerText = this.name
        gender.innerText = this.gender
        birthYear.innerText = this.birthYear

        character.append(name,gender,birthYear)
        // character.append(name)
        parentElem.insertAdjacentElement('beforeend',character)
    }
}