const url = 'https://ajax.test-danit.com/api/swapi/',
    heads = {'Content-type': 'application/json'}

//preloader
const preloader = new Preloader(document.body)
preloader.show()
//preloader


const films = fetch(`${url}films`,{
    method: 'get',
    headers: heads
})

filmsList = document.createElement('div')
filmsList.className = 'film'
document.getElementById('root').append(filmsList)


films
.then(resp => {
    if(resp.status === 200)
    {
        return resp.json()
    }
})
.then(films => {
    console.log(films)
    films.forEach(film => {
        const fi = new Film(film)
        fi.render(filmsList)
        preloader.hide()
    });
})
.catch(e => console.error(e))